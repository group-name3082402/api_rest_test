package TestPackage;

import java.io.File;
import java.io.IOException;
import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Environment;
import Repository.RequestBody;
import io.restassured.response.Response;

public class Test_Case_B1 extends RequestBody {

	public static void executor() throws ClassNotFoundException, IOException {
		
		File dir_name = Utility.CreateLogDirectory("Get_API_Logs");
		String requestBody = RequestBody.req_get_tc("Get_TC4");
		String Endpoint = Environment.Hostname() + Environment.Resource_get() + requestBody;
		int statuscode=0;

		for (int i = 0; i < 5; i++) {
			Response response = API_Trigger.Get_trigger(Endpoint);

			statuscode = response.statusCode();

			if (statuscode == 404) {
				Utility.evidenceFileCreator(Utility.testLogName(tstlst.Testname()), dir_name, Endpoint, requestBody,
						response.getHeader("Date"), response.getBody().asString());
				break;
			}
			else {
				System.out.println("Expected status code is not found in current iteration :" +i+ " hence retrying");
			}
		
		if (statuscode!=404) {
			System.out.println("Expected status code not found even after 5 retries hence failing the test case");
			Assert.assertEquals(statuscode, 404);
		}
	}
	}
}
