package TestPackage;

import java.io.File;
import java.io.IOException;
import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Environment;
import Repository.RequestBody;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Test_Case_B0 extends RequestBody {
	
	public static void executor() throws ClassNotFoundException, IOException {
		
		File dir_name = Utility.CreateLogDirectory("Get_API_Logs");
		String requestBody = RequestBody.req_get_tc("Get_TC3");
		String Endpoint = Environment.Hostname() + Environment.Resource_get() + requestBody;
		int statuscode=0;

		for (int i = 0; i < 5; i++) {
			Response response = API_Trigger.Get_trigger(Endpoint);

			statuscode = response.statusCode();

			if (statuscode == 200) {
				Utility.evidenceFileCreator(Utility.testLogName(tstlst.Testname()), dir_name, Endpoint, requestBody,
						response.getHeader("Date"), response.getBody().asString());
				validator(response);
				break;
			}
			else {
				System.out.println("Expected status code is not found in current iteration :" +i+ " hence retrying");
			}

		}
		
		if (statuscode!=200) {
			System.out.println("Expected status code not found even after 5 retries hence failing the test case");
			Assert.assertEquals(statuscode, 200);
		}
		}
		public static void validator(Response response) {
			ResponseBody res_body = response.getBody();


		// Step 5.2 : Parse individual params using jsp_res object 
        String res_id=res_body.jsonPath().getString("data.id");
		String res_email=res_body.jsonPath().getString("data.email");
		String res_first_name=res_body.jsonPath().getString("data.first_name");
		String res_last_name=res_body.jsonPath().getString("data.last_name");
		String res_avatar=res_body.jsonPath().getString("data.avatar");
		
		
		//step  6.0 store expected result
		String exp_id="1";
		String exp_email="george.bluth@reqres.in";
		String exp_first_name="George";
		String exp_last_name="Bluth";
		String exp_avatar="https://reqres.in/img/faces/1-image.jpg";
				 
		
		// Step 6 : Validate the response body

		// Step 6.2 : Use TestNG's Assert   
		Assert.assertEquals(res_id, exp_id);
		Assert.assertEquals(res_email,exp_email );
		Assert.assertEquals(res_first_name, exp_first_name);
		Assert.assertEquals(res_last_name, exp_last_name);
		Assert.assertEquals(res_avatar, exp_avatar);
	}


}

