Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Vaishnavi",
    "job": "qalead"
}

Response header date is : 
Sun, 25 Feb 2024 13:47:48 GMT

Response body is : 
{"name":"Vaishnavi","job":"qalead","id":"552","createdAt":"2024-02-25T13:47:48.035Z"}