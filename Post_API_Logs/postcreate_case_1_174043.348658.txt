Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Wed, 28 Feb 2024 12:10:43 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"715","createdAt":"2024-02-28T12:10:43.048Z"}