Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "vinayak",
    "job": "qa"
}

Response header date is : 
Wed, 28 Feb 2024 12:10:51 GMT

Response body is : 
{"name":"vinayak","job":"qa","updatedAt":"2024-02-28T12:10:51.269Z"}