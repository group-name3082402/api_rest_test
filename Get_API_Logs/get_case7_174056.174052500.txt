Endpoint is :
https://reqres.in/api/unknown/2

Request body is :
/unknown/2

Response header date is : 
Wed, 28 Feb 2024 12:10:56 GMT

Response body is : 
{"data":{"id":2,"name":"fuchsia rose","year":2001,"color":"#C74375","pantone_value":"17-2031"},"support":{"url":"https://reqres.in/#support-heading","text":"To keep ReqRes free, contributions towards server costs are appreciated!"}}