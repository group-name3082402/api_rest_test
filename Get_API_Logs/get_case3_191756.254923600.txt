Endpoint is :
https://reqres.in/api/users/1

Request body is :
/users/1

Response header date is : 
Sun, 25 Feb 2024 13:47:56 GMT

Response body is : 
{"data":{"id":1,"email":"george.bluth@reqres.in","first_name":"George","last_name":"Bluth","avatar":"https://reqres.in/img/faces/1-image.jpg"},"support":{"url":"https://reqres.in/#support-heading","text":"To keep ReqRes free, contributions towards server costs are appreciated!"}}